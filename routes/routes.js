module.exports = function(app)
{   
    app.get('/create',function(req,res){
        require('../create');
        res.send('Created campaign');
     });
    app.get('/read', function(req,res){
      require('../read');
      res.send('Read campaign');
    });
    app.get('/update', function(req,res){
      require('../update');
      res.send('Updated campaign');
    });
    app.get('/delete', function(req,res){
      require('../delete');
      res.send('Deleted campaign');
    });
}